import './App.css';
import players from './players';
import Player from './components/Player/Player';
import PlayerList from './components/Player/PlayerList';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {

  let userName = "";
  return (
    <div className="App">
      <div className='d-flex flex-column flex-wrap justify-content-center align-items-center'>
      <Title title={"Inazuma eleven Player Card"} />
      <PlayerList />
    </div>
    </div>
  );
}

export default App;

function Title({title = null}) {
  return <div className='text-center'>
  <h1>{title}</h1>
  <hr/>
  </div>
}