import React from 'react';
import Player from '../Player/Player'
import 'bootstrap/dist/css/bootstrap.min.css';
import players from '../../players';


const PlayerList = () => {
    return (
        <div className='d-flex flex-wrap gap-3'>
        {players.map((player,key) => (
          <Player  key={key} props={player}></Player>
        ))}
      </div>
    );
}

export default PlayerList;


