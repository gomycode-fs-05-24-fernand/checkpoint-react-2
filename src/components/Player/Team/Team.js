import React from 'react';

const Team = ({team} = null) => {
    return (
        <h4>
            {team}
        </h4>
    );
}

export default Team;