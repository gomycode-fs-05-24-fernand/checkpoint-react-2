import React from 'react';

const JerseyNumber = ({jerseyNumber = null}) => {
    return (
        <p>
            JerseyNumber: {jerseyNumber}
        </p>
    );
}

export default JerseyNumber;
