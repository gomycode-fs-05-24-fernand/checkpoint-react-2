import React from 'react';
import './image.css';

const Image = ({image=""}) => {
    return (
        <img src={image} className='m-5 image-top-card player-image' style={{width:'10rem'}}/>
    );
}

export default Image;
