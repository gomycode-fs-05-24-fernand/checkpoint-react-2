import React from 'react';
import Name from './Name/Name';
import Nationality from './Nationality/Nationality';
import Team from './Team/Team';
import Age from './Age/Age';
import JerseyNumber from './JerseyNumber/JerseyNumber';
import Image from './Image/Image';
import Card from 'react-bootstrap/Card';
import 'bootstrap/dist/css/bootstrap.min.css';


const Player = ({props}) => {
    return (
<Card className="mb-3 shadow-sm">
            <Image image={props.imageUrl} />
            <Card.Body className="text-center">
                <Card.Title>
                    <Name name={props.name}/>
                </Card.Title>
                <Card.Subtitle className="mb-2 text-muted">
                    <Team team={props.team}/>
                </Card.Subtitle>
                <Card.Text>
                    <strong>Nationality:</strong> <Nationality nationality={props.nationality}/> <br/>
                    <strong>Age:</strong> <Age age={props.age}/> <br/>
                    <strong>Jersey Number:</strong> <JerseyNumber jerseyNumber={props.jerseyNumber}/>
                </Card.Text>
            </Card.Body>
        </Card>
    );
}

export default Player;
