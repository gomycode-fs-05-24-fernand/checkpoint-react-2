import React from 'react';

const Nationality = ({nationality=""}) => {
    return (
        <h4>
            {nationality}
        </h4>
    );
}

export default Nationality;
